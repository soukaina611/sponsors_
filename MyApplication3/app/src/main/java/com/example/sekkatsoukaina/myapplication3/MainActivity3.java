package com.example.sekkatsoukaina.myapplication3;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class MainActivity3 extends AppCompatActivity {


    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view;
        view = inflater.inflate(R.layout.activity_main3,container,false);
        return view;
    }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
    }
    public void sponsors1(View view){
        Intent sponsors1Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.cnrs.fr/"));
        startActivity(sponsors1Intent);
    }

    public void sponsors2(View view){
        Intent sponsors2Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.lis-lab.fr//"));
        startActivity(sponsors2Intent);
    }
    public void sponsors3(View view){
        Intent sponsors3Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://icube-web.unistra.fr/gdr-igrv/index.php/Accueil//"));
        startActivity(sponsors3Intent);
    }
    public void sponsors4(View view){
        Intent sponsors4Intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://g-mod.lis-lab.fr///"));
        startActivity(sponsors4Intent);
    }
}
